

# install.packages("rgdal")
# install.packages("rgeos")
# install.packages("raster")
# install.packages("parallel")
library(rgdal)
library(rgeos)
library(raster)
library(parallel)

############ From Launch_ttMarket.R ############
setwd("~/Documents/Kyo/Analysis/TropAtl_RunRR")
path = "~/Documents/Kyo/Analysis/TropAtl_RunRR"
pathBuffer = "~/Documents/Kyo/Analysis/TropAtl_RunRR"
Reef = "ReefsB/reefB.shp"
tt = "friction_b"
ref = "+proj=cea +lat_ts=30"
Cities = "CitiesB/citiesB.shp"
distance = "linearDistance_reefs_cities_tropAtlRR.csv"
############ From Launch_ttMarket.R ############

############ From Calcul_ttMarket.R ############
source("AnnexFunctionReef.R")
require(rgdal)
require(raster)
require(rgeos)
require(gdistance)
setwd(path)
layer <- ogrListLayers(Reef)
reef<-readOGR(Reef,layer)
proj4string(reef) = CRS(ref)
layer <- ogrListLayers(Cities)
cities<-readOGR(Cities,layer)
proj4string(cities) = CRS(ref)
travel_time<-raster(tt)
proj4string(travel_time) = CRS(ref)
pool = reef@data #Load the values file that was defined as the object "reef"
coord = coordinates(reef)
dist = read.csv(distance)
############ From Calcul_ttMarket.R ############

############ From ResReef.R ############

ResReef<-function(i,coord,cities,travel_time,hour=12){  
  
  a = Sys.time()
  # Identify the site (the reef)
  site = coord[i,]
  FID_site = pool[i,"reef_ID"] #Choose "i" value from column "reef_ID" of database "pool"
  dist_site = subset(dist,dist[,"InputID"]==FID_site)
  #dist_site = subset(dist,dist[,1]==0)   Another notation w/the same effect in this unique case (for reef #0)
  dist_site_order = dist_site[order(dist_site[,"Distance"]),] #Order entries using "Distance" values as ordering criterion
  #dim(dist_site_order)   Returns dimensions of data matrix
  Res=NULL
  
  for(j in 1:dim(dist_site_order)[1]){
    
    cat("j",j,"\n") 
    # The define our working radius, the distance from the reef to the nearest city + 100km
    R = dist_site_order[j,"Distance"]+100000
    
    # To define the interval, useful for the cities that are situated at the border (-180, 180)
    intervalle = .getIntervalle(site,R)
    
    # Transform the site coordinates into a spatial points (t is transpose)
    #??????????????WHY was it transposed? => to transform the coordinates into a matrix by row
    siteSP = SpatialPoints(t(site))
    proj4string(siteSP) = CRS(ref)
    proj4string(siteSP)
    
    # Longitude and latitude of the friction grid
    long = .getlong(travel_time)
    lat = .getlat(travel_time)
    
    
    # To select our targeted city
    #dist_site_order[1,"TargetID"]
    #which(cities@data[,"ObjectID"] == dist_site_order[1,"TargetID"])
    Targetcity = cities[which(cities@data[,"ObjectID"] == dist_site_order[j,"TargetID"]),]
    coord_city=coordinates(Targetcity)
    
    # To transform the reef and the targeted city into Spatial Points
    pts = SpatialPoints(rbind(site,coord_city))
    proj4string(pts) = CRS(ref)
    
    # Create the buffer around the reef with the radius R
    if (ncol(intervalle) == 2) {
      buffer = gBuffer(pts[1,],width=R,quadseg=10000) 
      
      # Extract the part of the friction grid corresponding the the buffer
      Rast = .getRast(travel_time,buffer) 
      
    } else { 
      
      Rast = .getRast2(travel_time,intervalle)   
      
    }
    
    # Review the raster extracted and the position of the points within the raster
    #plot(travel_time)
    #plot(Rast)
    #plot(pts,add=TRUE) #Plots the points OVER the previous plot (the raster)
    
    # To determine in which pixel is the reef
    long = .getlong(Rast)
    lat = .getlat(Rast)
    pixel_ss=.getPixelSS(long,lat,site)
    
    # To determine in which pixel is the city
    if (ncol(intervalle) == 2) {
      pixels_city=.getPixelCities(long,lat,coord_city)
      
    } else {
      
      pixels_city=.getPixelCities2(site,long,lat,coord_city,travel_time)
      
    } # end of else
    
    
    # Transition matrix
    #????HOW is this calculated again? It's recalling the values of the friction grid?
    tr <- transition(Rast, function(x) 1/mean(x), 8)
    tr <- geoCorrection(tr)
    Acc_cost <- accCost(tr, pts[1,])
    #Acc_cost
    #plot(Acc_cost)
    
    # Determine the cost to go from the reef to the targeted city
    costCity = Acc_cost[pixels_city[,1],pixels_city[,2]]
    #???Values?
    
    ID_city = dist_site_order[j,"TargetID"]
    dist = dist_site_order[j,"Distance"]
    tps_traj = costCity
    pop_city = Targetcity@data$POP
    gravity = pop_city/(tps_traj^2)
    
    
    #to get the friction values on the raster
    ttvalues = getValues(Rast) 
    
    # path between the reef and the targeted city
    path_tot = shortestPath(tr, pts[1,], pts[2,]) 
    
    # to get the pixels on the path
    values_tot = getValues(raster(path_tot))
    pixels = which(is.na(values_tot)==F)
    
    
    if (length(pixels)>0){
      
      friction = ttvalues[pixels]
      costvalues = getValues(Acc_cost)
      cost = costvalues[pixels]
      
      rankcost = cost[order(cost)]
      rankfriction = friction[order(cost)]
      
      link=vector()
      for (k in 1:length(pixels)-1){
        link[k]=2*(rankcost[k+1]-rankcost[k])/(rankfriction[k]+rankfriction[k+1])
      }
      
      road = which(rankfriction == 0.001 )
      trail = which(rankfriction == 0.002 )
      road_position = c(road,trail)
      
      sea = which(rankfriction == 0.003)
      veg = length(which(rankfriction != 0.001 & rankfriction !=0.002 & rankfriction != 0.003))
      
      Dcell = 0
      for (f in 1:length(link)){
        Dcell[f] = Dcell[f]+ link[f]/2
        Dcell[f+1] = link[f]/2
      }
      
      dist_tot = sum(Dcell)/1000
      dist_road = (sum(Dcell[(road_position)])/1000)
      dist_sea = (sum(Dcell[sea])/1000)
      dist_veg = (sum(Dcell[veg])/1000)
      
      Res = rbind(Res,c(FID_site,ID_city,dist,tps_traj,pop_city,gravity,dist_road,dist_sea,dist_veg,dist_tot))
      
      
    }else{
      
      dist_road = dist_sea = dist_veg = dist_tot = 0
      Res = rbind(Res,c(FID_site,ID_city,dist,tps_traj,pop_city,gravity,dist_road,dist_sea,dist_veg,dist_tot))
      
    }
    if(dim(Res)[1]>=5){
      if(Res[j,4]>=hour*60&Res[(j-1),4]>=hour*60&Res[(j-2),4]>=hour*60&Res[(j-3),4]>=hour*60&Res[(j-4),4]>=hour*60){
        
        break
      }
    }
    
  }
  b = Sys.time()
  cat("i",i,": time",b-a,"\n") 
  colnames(Res) = c("reefID","cityID","LinearDistance","TravelTime","population","gravity","roadDistance","seaDistance","vegetationDistance","totalDistance")
  
  save(Res,file=paste0("ResultsbackUp_local/Res_Reef.Rdata",FID_site))
  return(Res)
}

