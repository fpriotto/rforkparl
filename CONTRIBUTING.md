Well, the distance_calculator-FP.R script should set a fork cluster for sharing the requirement environment to run the ResReef function in a multi-treading computation...

1) Start
R CMD BATCH distance_calculator-FP.R 

2) See logs in distance_calculator-FP.log

3) If you are optimist, see the results files in TropAtl_RunRR/ResultsbackUp/ directory